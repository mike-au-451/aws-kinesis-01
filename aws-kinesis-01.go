/*
Write data to an AWS/Kinesis stream
This should function as an AWS/Lambda handler


*/

package main

import (
	"context"
	"fmt"
	"net/http"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/aws/credentials"

	"github.com/aws/aws-sdk-go/service/kinesis"

	"github.com/joho/godotenv"
)

var (
	awsIAMName string
	awsCreds *credentials.Credentials
	awsConfig *aws.Config
	awsSession *session.Session
	awsKinesisStream string
	awsKinesisKey string
)

func main() {

	var (
		ok bool
	)

	if !args() {
		fmt.Printf("FATAL: bad args\n")
		return
	}

	if err := godotenv.Load(); err != nil {
		fmt.Printf("FATAL: failed to load .env: %s\n", err)
		return
	}

	if awsIAMName, ok = getenv("AWS_IAM_NAME", ""); !ok || awsIAMName == ""{
		fmt.Printf("FATAL: failed to get IAM name\n")
		return
	}

	if awsKinesisStream, ok = getenv("AWS_KINESIS_NAME", ""); !ok || awsKinesisStream == "" {
		fmt.Printf("FATAL: failed to get stream name\n")
		return
	}

	if awsKinesisKey, ok = getenv("AWS_KINESIS_KEY", ""); !ok || awsKinesisKey == "" {
		fmt.Printf("FATAL: failed to get stream key\n")
		return
	}

	awsCreds = credentials.NewSharedCredentials("credentials", awsIAMName)
	if awsCreds == nil {
		fmt.Printf("FATAL: failed to get credentials for " + awsIAMName)
		return
	}

	awsConfig = aws.NewConfig().WithCredentials(awsCreds)
	if awsConfig == nil {
		fmt.Printf("FATAL: failed to get config for " + awsIAMName)
		return
	}

	awsSession = session.New(awsConfig)
	if awsSession == nil {
		fmt.Printf("FATAL: failed to get aws session for " + awsIAMName)
		return
	}

	lambda.Start(Handler)
}

func args() bool {
	return true
}

func getenv(key, fallback string) (string, bool) {

	var val string

	if key == "" {
		fmt.Printf("BUG: getenv: missing key\n")
		return "", false
	}

	if val = os.Getenv(key); val == "" {
		val = fallback
	}

	return val, true
}

func Handler(ctx context.Context, evt events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	var err error

	apiResponseOK := events.APIGatewayProxyResponse{
		StatusCode: http.StatusOK,
		Body: "",
	}

	apiResponseMethodNotAllowed := events.APIGatewayProxyResponse{
		StatusCode: http.StatusMethodNotAllowed,
		Body: "",
	}

	apiResponseInternalServerError := events.APIGatewayProxyResponse{
		StatusCode: http.StatusInternalServerError,
		Body: "",
	}

	if evt.HTTPMethod != http.MethodPost {
		fmt.Printf("ERROR: method not allowed: %s\n", evt.HTTPMethod)
		return apiResponseMethodNotAllowed, nil
	}

	if err = kinesisDump(awsSession, evt.Body); err != nil {
		fmt.Printf("ERROR: kinesis dump failed\n")
		return apiResponseInternalServerError, nil
	}

	return apiResponseOK, nil
}

func kinesisDump(awsSession *session.Session, blob string) error {

	var (
		kh *kinesis.Kinesis
		ksn, key *string
		//ks *kinesis.CreateStreamOutput
		put *kinesis.PutRecordOutput
		err error
	)

	kh = kinesis.New(awsSession)
	ksn = aws.String(awsKinesisStream)
	key = aws.String(awsKinesisKey)
/*
	ks, err = kh.CreateStream(
		&kinesis.CreateStreamInput{
			ShardCount: aws.Int64(1),
			StreamName: ksn,
		})
	ks = ks // TODO: how is this supposed to be used?
	if err != nil {
		fmt.Printf("ERROR: failed to create stream: %s\n", err)
		return err
	}
*/
	err = kh.WaitUntilStreamExists(
		&kinesis.DescribeStreamInput{
			StreamName: ksn,
		})
	if err != nil {
		fmt.Printf("ERROR: stream does not exist: %s\n", err)
		return err
	}

	put, err = kh.PutRecord(
		&kinesis.PutRecordInput{
			Data: []byte(blob),
			StreamName: ksn,
			PartitionKey: key,
		})
	put = put // TODO: how is this supposed to be used?
	if err != nil {
		fmt.Printf("ERROR: PutRecord failed: %s\n", err)
		return err
	}

/* TODO:
1.  Does the stream (ks) get used?
2.  Does there need to be clean up?

See
https://github.com/suzuken/amazon-kinesis-go-example/blob/master/main.go
*/

	return nil
}
