#!/bin/bash

# Read from an AWS/Kinesis stream

AWS_STREAM_NAME=${1:-apigStream}
#echo "AWS_STREAM_NAME: $AWS_STREAM_NAME"

# Example describe-stream
# {
#   "StreamDescription": {
#     "Shards": [
#       {
#         "ShardId": "shardId-000000000000",
#         "HashKeyRange": {
#           "StartingHashKey": "0",
#           "EndingHashKey": "340282366920938463463374607431768211455"
#         },
#         "SequenceNumberRange": {
#           "StartingSequenceNumber": "49593395189000377465300579171233280692851086625293205506"
#         }
#       }
#     ],
#     "StreamARN": "arn:aws:kinesis:ap-southeast-2:002096802903:stream/apigStream",
#     "StreamName": "apigStream",
#     "StreamStatus": "ACTIVE",
#     "RetentionPeriodHours": 24,
#     "EnhancedMonitoring": [
#       {
#         "ShardLevelMetrics": []
#       }
#     ],
#     "EncryptionType": "NONE",
#     "KeyId": null,
#     "StreamCreationTimestamp": 1551343569
#   }
# }

# Example get-records
# {
#     "Records": [
#         {
#             "SequenceNumber": "49593395189000377465300579202265197556539475906984411138",
#             "ApproximateArrivalTimestamp": 1551350450.568,
#             "Data": "ewogICAgImZvbyI6ImJhciIsCiAgICAiYmluZyI6ImJhbmciCn0=",
#             "PartitionKey": "blobs"
#         },
#         {
#             "SequenceNumber": "49593395189000377465300579202266406482359096171156209666",
#             "ApproximateArrivalTimestamp": 1551350532.116,
#             "Data": "ewogICAgImZvbyI6ImJhciIsCiAgICAiYmluZyI6ImJhbmciCn0=",
#             "PartitionKey": "blobs"
#         }
#     ],
#     "NextShardIterator": "AAAAAAAAAAFAlgYr8/oa10aM4i4HGhv/8YWN2Zny+T1eKHJxCbpaUx9CG2UR7i7XlgcbHfVCsyDSnMquikYuQqgaiE0rlZNsQEFG6LQ8cJyjMVQkGycOX9W9vDPmaZmN8bys+Vzr/PK9Rg6WnN6eDlt3zA9PcbvvG+y7QQ8A3O6Rm6wj+lls9Hh/YK3/A/ASLePYrjaJHmq/JzGXPu9/3TncTiXSufG3",
#     "MillisBehindLatest": 0
# }

SHARDS=`aws --profile Administrator --region ap-southeast-2 kinesis describe-stream --stream-name $AWS_STREAM_NAME | jq '.StreamDescription.Shards'`
SHARD_CNT=`echo $SHARDS | jq 'length'`

for ((ii=0; ii<$SHARD_CNT; ii++))
do
	SHARD_ID=`echo $SHARDS | jq -r ".[$ii].ShardId"`
	SHARD_ITER=`aws --profile Administrator --region ap-southeast-2 kinesis get-shard-iterator --stream-name $AWS_STREAM_NAME --shard-id $SHARD_ID --shard-iterator-type TRIM_HORIZON | jq -r '.ShardIterator'`

	while [[ -n "$SHARD_ITER" ]]
	do
		RECORDS=`aws --profile Administrator --region ap-southeast-2 kinesis get-records --shard-iterator $SHARD_ITER`
		NEXT_ITER=`echo $RECORDS | jq -r '.NextShardIterator'`
		RECORDS=`echo $RECORDS | jq -r '.Records'`
		RECORD_CNT=`echo $RECORDS | jq 'length'`
		if [[ $RECORD_CNT -eq 0 ]]
		then
			echo "no more records"
			break
		fi

		for ((jj=0; jj<$RECORD_CNT; jj++))
		do
			DATA=`echo $RECORDS | jq -r ".[$jj].Data"`
			echo $DATA | base64 --decode | jq
		done

		SHARD_ITER=$NEXT_ITER
	done
done

