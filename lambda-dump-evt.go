/*
Dump an AWS event.
*/

package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kinesis"
)

const (
	awsRegion = "ap-southeast-2"

	kinesisStream = "apigStream"
	kinesisPartition = "blobs"
)

var (
	rspOK, rspBad, rspErr events.APIGatewayProxyResponse

	awsSession *session.Session
)

func main() {
	var (
		err error
	)

	rspOK = events.APIGatewayProxyResponse{
			Body: "",
			StatusCode: 200,
		}

	rspBad = events.APIGatewayProxyResponse{
			Body: "",
			StatusCode: 400,
		}

	rspErr = events.APIGatewayProxyResponse{
			Body: "",
			StatusCode: 500,
		}

	awsSession, err = session.NewSession(
		&aws.Config{
			Region: aws.String(awsRegion),
		})
	if err != nil {
		fmt.Printf("FATAL: main: failed to get session: %s\n", err)
		return
	}

	fmt.Printf("INFO: main: starting handler\n")
	lambda.Start(handler)
}

func handler(ctx context.Context, evt events.APIGatewayProxyRequest) (rsp events.APIGatewayProxyResponse, err error) {
//	fmt.Printf("evt:\n%+v\n", evt)

	if err = dumpToKinesis(evt.Body); err != nil {
		fmt.Printf("ERROR: dump failed\n")
		return rspErr, err
	}

	fmt.Printf("INFO: handler: succeded\n")

	return rspOK, nil
}

func dumpToKinesis(blob string) (err error) {

	var (
		kh *kinesis.Kinesis
		pri *kinesis.PutRecordInput
		pro *kinesis.PutRecordOutput
	)

	kh = kinesis.New(awsSession)
	if kh == nil {
		fmt.Printf("ERROR: dumpToKinesis: New: %s\n", err)
		return
	}

	pri = &kinesis.PutRecordInput{}
	pri.SetData([]byte(blob))
	pri.SetStreamName(kinesisStream)
	pri.SetPartitionKey(kinesisPartition)
	pro, err = kh.PutRecord(pri)
	if err != nil {
		fmt.Printf("ERROR: dumpToKinesis: PutRecord: %s\n", err)
	}

	fmt.Printf("INFO: dumpToKinesis: putted:\n%+v\n", pro)

	return
}

//func New(p client.ConfigProvider, cfgs ...*aws.Config) *Kinesis